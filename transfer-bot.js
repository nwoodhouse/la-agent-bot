"use strict";

/**
 * 
 * This bot illustrates how to build a simple agent bot, suitable for Live Assist for Dynamics
 * 365. On receipt of a message, it:
 * 1. Checks for the presence of 'transfer to' at the start of the message. If present, it uses
 *    The CaféX Bot Agent Connector API to affect a transfer to the agent identified.
 *
 * 2. If 'transfer to' is not present at the start of the message, the bot simply echoes back 
 *    to the user the same message that the user sent.
 *
 * To use this bot, simply replace the content of your 'index.js' with this.
 */


var builder = require("botbuilder");
var botbuilder_azure = require("botbuilder-azure");
var path = require('path');

const TRANSFER_MESSAGE = 'transfer to ';

var useEmulator = (process.env.NODE_ENV == 'development');

var connector = useEmulator ? new builder.ChatConnector() : new botbuilder_azure.BotServiceConnector({
    appId: process.env['MicrosoftAppId'],
    appPassword: process.env['MicrosoftAppPassword'],
    stateEndpoint: process.env['BotStateEndpoint'],
    openIdMetadata: process.env['BotOpenIdMetadata']
});

var bot = new builder.UniversalBot(connector);
bot.localePath(path.join(__dirname, './locale'));

bot.dialog('/', function (session) {
    
    switch(session.message.sourceEvent.type)
    {
        case "visitorContextData":
            // process context data if required. This is the first message received so say hello.
            session.send('Hi, I am an echo bot and will repeat everything you say.');
            break;

        case "systemMessage":
            // react to system messages if required
            break;

        case "transferFailed":
            // react to transfer failures if required
            session.send('Sorry - something went wrong when trying to transfer you.');
            break;

        case "otherAgentMessage":
            // react to messages from a supervisor if required
            break;

        case "visitorMessage":
            // Check for transfer message
			if(session.message.text.startsWith(TRANSFER_MESSAGE)) {
				var transferTo = session.message.text.substr(TRANSFER_MESSAGE.length);
				var msg = new builder.Message(session).sourceEvent({directline: {type: "transfer", agent: transferTo}});
				session.send(msg);
			} else {
				session.send('You said ' + session.message.text);
			}
            break;

        default:
            session.send('This is not a Live Assist message ' + session.message.sourceEvent.type);
    }
    
});

if (useEmulator) {
    var restify = require('restify');
    var server = restify.createServer();
    server.listen(3978, function() {
        console.log('test bot endpont at http://localhost:3978/api/messages');
    });
    server.post('/api/messages', connector.listen());    
} else {
    module.exports = { default: connector.listen() }
}